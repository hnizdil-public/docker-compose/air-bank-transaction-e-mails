import {main, MessagePartGetter, Row} from "./main.js"
import {FetchMessageObject} from "imapflow";

type AirbankRow = {
	date: string,
	transaction: number,
	type?: string,
	name?: string,
	account?: string,
	id?: string,
	info?: string,
	amount: number,
	balance: number,
	variableSymbol?: string,
	constantSymbol?: string,
}

function parseAmount(amount: string): number {
	return parseFloat(
		amount
			.replace(/[^0-9,]/g, '')
			.replace(',', '.')
	)
}

async function getter(_message: FetchMessageObject, messagePartGetter: MessagePartGetter): Promise<Row[]> {
	const {content} = await messagePartGetter('1')
	const body = content.toString()
	const row: AirbankRow = {
		date: "",
		transaction: 0,
		type: undefined,
		name: undefined,
		account: undefined,
		variableSymbol: undefined,
		constantSymbol: undefined,
		id: undefined,
		info: undefined,
		amount: 0,
		balance: 0,
	}

	const accountMatches = body.match(/úhrada (na účet|z účtu) (.*)?číslo ([-0-9]+\/\d+)/)
	if (accountMatches) {
		row.name = accountMatches[2]?.trim()
		row.account = accountMatches[3]
	} else {
		const infoMatches = body.match(/Pro úplnost uvádíme detaily této úhrady:\n\n(.*)/)
		if (infoMatches) {
			row.info = infoMatches[1]
		}
	}

	const [_, date, time, balance] = body.match(/zůstatek k (\d+\.\d+\.\d+) v (\d+:\d+) je ([\d, ]+)/)
	if (date && time && balance) {
		row.date = `${date} ${time}`
		row.balance = parseAmount(balance)
	}

	let messageSender: string | undefined
	let messageRecipient: string | undefined
	for (const [, key, value] of body.matchAll(/(.+?): (.+)/g)) {
		switch (key) {
			case 'Zpráva pro plátce':
				messageSender = value
				break
			case 'Zpráva pro příjemce':
				messageRecipient = value
				break
			case 'Částka':
				row.amount = parseAmount(value)
				break
			case 'Kód transakce':
				row.transaction = parseInt(value)
				break
			case 'Variabilní symbol':
				row.variableSymbol = value
				break
			case 'Konstantní symbol':
				row.constantSymbol = value
				break
		}
	}

	const isOutgoing = body.includes('se snížil o částku')
	if (isOutgoing) {
		row.amount *= -1
	}

	if (messageSender && messageRecipient) {
		if (isOutgoing) {
			row.id = messageSender
		} else {
			row.id = messageRecipient
		}
	} else {
		row.id = messageSender ?? messageRecipient
	}

	return [row]
}

function sorter(rows: Row[]): Row[] {
	return rows.sort((a: AirbankRow, b: AirbankRow) => a.transaction - b.transaction)
}

await main(getter, sorter)
