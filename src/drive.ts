import {google} from 'googleapis'
import config from './config.js'

export const drive = google.drive({
	version: "v3",
	auth: new google.auth.GoogleAuth({
		keyFile: config.keyFile,
		scopes: 'https://www.googleapis.com/auth/drive',
	})
})
