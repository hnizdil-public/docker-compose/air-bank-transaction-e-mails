import {FetchMessageObject} from "imapflow";
import {Row} from "./main";

export class Thread {
	readonly id: string
	readonly lastMessage: FetchMessageObject
	readonly rows: Row[]

	constructor(id: string, messages: FetchMessageObject[], rows: Row[]) {
		this.id = id
		this.lastMessage = messages.pop()
		this.rows = rows
	}
}
