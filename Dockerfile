FROM node:20.5.0-alpine3.18 AS node

ENV ROOT=/home/node
USER node:node
WORKDIR $ROOT
ENTRYPOINT ["node"]

FROM node as npm

COPY --chown=node:node package*.json ./
COPY packages/ packages/
RUN npm install --omit=dev --workspaces=false

FROM npm as npm-csob

RUN npm install --omit=dev --workspace=csob

FROM npm as build

RUN npm install

COPY src/ src/
COPY tsconfig.json ./

# --max-old-space-size is required for tsc to work on Raspberry Pi 4 B
RUN NODE_OPTIONS=--max-old-space-size=1500 npx tsc --sourceMap false

RUN mkdir csob
COPY csob/csob.xslt csob/
RUN npm run sef

FROM node AS app

COPY --chown=node:node package*.json ./
COPY --chown=node:node --from=npm ${ROOT}/node_modules/ node_modules/
COPY --chown=node:node --from=build ${ROOT}/build/ build/

FROM app AS airbank

FROM app AS csob

USER root
RUN apk --no-cache add libxml2-utils tzdata
RUN ln -s /usr/share/zoneinfo/Europe/Prague /etc/localtime
USER node:node

RUN mkdir csob
COPY --chown=node:node --from=build ${ROOT}/csob/csob.sef.json csob/
COPY --chown=node:node --from=npm-csob ${ROOT}/node_modules/ node_modules/

FROM app AS payslip

USER root
RUN apk --no-cache add poppler-utils p7zip
USER node:node
